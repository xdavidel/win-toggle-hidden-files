# Toggle Hidden Files in Windows

## Installtion
1. Clone this repository:
```
git clone https://gitlab.com/xdavidel/win-toggle-hidden-files
```

2. Open command prompt in the repo directory
```
xcopy "ToggleHiddenFiles.exe" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\"
```

3. To toggle hidden files (from anywhere)
```
<Win> + K
```

## Credits
https://www.howtogeek.com/howto/keyboard-ninja/keyboard-ninja-toggle-hidden-files-with-a-shortcut-key-in-windows/
